<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/9/18
 * Time: 6:28 PM
 */

namespace MereHead\WalletsModuleConnector\WalletsServices;


trait ExchangeService
{
    public function getExchangeHistory(int $account_id, int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'account_id'   => $account_id,
            'current_page' => $current_page,
            'per_page'     => $per_page,
        ];

        return $this->makeCallGuzzle('GET', 'exchange_history', $body);
    }

    public function getExchangeRate(int $fromAssetId, int $toAssetId)
    {
        $body = [
            'from_asset_id' => $fromAssetId,
            'to_asset_id' => $toAssetId,
        ];

        return $this->makeCallGuzzle('GET', 'exchange_rate', $body);
    }

    public function makeExchange(int $fromAssetId, int $toAssetId, float $quantity, int $account_id)
    {
        $body = [
            'account_id' => $account_id,
            'from_asset_id' => $fromAssetId,
            'to_asset_id' => $toAssetId,
            'quantity' => $quantity,
        ];

        return $this->makeCallGuzzle('POST', 'make_exchange', $body);
    }
}
