<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/15/18
 * Time: 3:46 PM
 */

namespace MereHead\WalletsModuleConnector\WalletsServices;


trait Web3Service
{
    /**
     * Send tokens to wallet
     * @param int $account_id
     * @return mixed
     */
    public function sendRerRegisterTokens(int $account_id){
        $msg  = [
            'commands' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id,
            ]
        ];
        return $this->makeCall($msg);
    }



    public function sendFrom(int $account_id, string $from, string $to, $amount){
        $msg  = [
            'commands' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'from' => $from,
                'to' => $to,
                'amount' => $amount
            ]
        ];
        return $this->makeCall($msg);
    }
}