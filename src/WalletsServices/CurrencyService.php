<?php


namespace MereHead\WalletsModuleConnector\WalletsServices;

trait CurrencyService
{
    public function getAssetsInfo()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'assets_info', $body);
    }

    public function getAssetsStatus()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'assets_status', $body);
    }
}
