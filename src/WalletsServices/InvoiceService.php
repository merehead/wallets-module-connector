<?php


namespace MereHead\WalletsModuleConnector\WalletsServices;


trait InvoiceService
{
    public function getInvoiceWithdrawals(int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'current_page' => $current_page,
            'per_page' => $per_page
        ];

        return $this->makeCallGuzzle('GET', 'invoice_withdrawals', $body);
    }

    public function getInvoiceDeposits(int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'current_page' => $current_page,
            'per_page' => $per_page
        ];

        return $this->makeCallGuzzle('GET', 'invoice_deposits', $body);
    }

    public function getInvoiceTransactions(int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'current_page' => $current_page,
            'per_page' => $per_page
        ];

        return $this->makeCallGuzzle('GET', 'invoice_transactions', $body);
    }

    public function deleteFiatDepositWithdrawalRequest(int $user_id, int $invoice_id)
    {
        $body = [
            'user_id'    => $user_id,
            'invoice_id' => $invoice_id
        ];

        return $this->makeCallGuzzle('DELETE', 'delete_invoice_request', $body);
    }


    public function getInvoice(int $id)
    {
        $body = [
            'invoice_id' => $id,
        ];

        return $this->makeCallGuzzle('GET', 'invoice', $body);
    }

    public function getUserInvoiceTransactions(int $account_id)
    {
        $body = [
            'account_id' => $account_id,
        ];

        return $this->makeCallGuzzle('GET', 'invoice_transactions', $body);
    }

    public function createFiatDepositRequest(array $requestData)
    {
        $body = [
            'request_data' => $requestData,
        ];

        return $this->makeCallGuzzle('POST', 'invoice_deposit', $body);
    }

    public function createFiatWithdrawalRequest(array $requestData)
    {
        $body = [
            'request_data' => $requestData,
        ];

        return $this->makeCallGuzzle('POST', 'invoice_withdrawal', $body);
    }

    public function createFiatWithdrawalRequestWithBankAccount(array  $requestData)
    {
        $body = [
            'request_data' => $requestData,
        ];

        return $this->makeCallGuzzle('POST', 'invoice_withdrawal_with_bank_account', $body);
    }

    public function updateInvoiceStatus(int $id, int $account_id, string $status, string $type, string $comment = null)
    {
        $body = [
            'invoice_id' => $id,
            'account_id' => $account_id,
            'status'     => $status,
            'type'       => $type,
            'comment'    => $comment,
        ];

        return $this->makeCallGuzzle('PATCH', 'invoice_status', $body);
    }
}