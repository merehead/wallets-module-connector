<?php

namespace MereHead\WalletsModuleConnector\WalletsServices;


trait WalletsService
{
    public function getAssets($params = null)
    {
        $body = [
            'params' => $params
        ];

        return $this->makeCallGuzzle('GET', 'assets', $body);
    }

    public function updateAsset(string $asset_code, array $data)
    {
        $body = [
            'asset_code' => $asset_code,
            'asset_data' => $data
        ];

        return $this->makeCallGuzzle('PUT', 'update_asset', $body);
    }

    public function updateAssetsArray(array $data)
    {
        $body = [
            'asset_data' => $data
        ];

        return $this->makeCallGuzzle('PUT', 'update_assets_array', $body);
    }

    public function updateAssetStatus(array $data)
    {
        $body = [
            'asset_data' => $data
        ];

        return $this->makeCallGuzzle('PUT', 'update_asset_status', $body);
    }

    public function getTokens()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'tokens', $body);
    }

    public function addToken(array $data)
    {
        $body = [
            'token_data' => $data
        ];

        return $this->makeCallGuzzle('POST', 'add_token', $body);
    }

    public function addTrxToken(array $data)
    {
        $body = [
            'token_data' => $data
        ];

        return $this->makeCallGuzzle('POST', 'add_trx_token', $body);
    }

    public function getWallets(int $account_id, $parent_id = null)
    {
        $body = [
            'account_id' => $account_id,
            'parent_id'  => $parent_id,
        ];

        return $this->makeCallGuzzle('GET', 'wallets', $body);
    }

    public function getTotalWalletsBalanceInBTC(int $account_id)
    {
        $body = [
            'account_id'  => $account_id
        ];

        return $this->makeCallGuzzle('GET', 'total_in_btc', $body);
    }

    public function getWallet(int $account_id, $wallet_id)
    {
        $body = [
            'account_id' => $account_id,
            'wallet_id' => $wallet_id
        ];

        return $this->makeCallGuzzle('GET', 'wallet', $body);
    }

    public function getBalanceForWallet(int $account_id, int $asset_id)
    {
        $body = [
            'account_id' => $account_id,
            'asset_id' => $asset_id
        ];

        return $this->makeCallGuzzle('GET', 'wallet_balance', $body);
    }

    public function generateAddressForWallet(int $account_id, $wallet_id)
    {
        $body = [
            'account_id' => $account_id,
            'wallet_id' => $wallet_id
        ];

        return $this->makeCallGuzzle('POST', 'generate_address', $body);
    }

    public function getTransactions(string $search_query = null)
    {
        $body = [
            'search_query' => $search_query,
        ];

        return $this->makeCallGuzzle('GET', 'transactions', $body);
    }

    /**
     * @param int $account_id
     * @param $wallet_id
     * @param int $current_page
     * @param int $per_page
     * @param string $filter_type Type of transactions (deposit or withdrawal)
     * @return mixed
     */
    public function getWalletTransactions(int $account_id, $wallet_id, int $current_page = 0, int $per_page = 15, string $filter_type = null)
    {
        $body = [
            'account_id' => $account_id,
            'wallet_id' => $wallet_id,
            'current_page' => $current_page,
            'per_page' => $per_page,
            'filter_type' => $filter_type,
        ];

        return $this->makeCallGuzzle('GET', 'wallet_transactions', $body);
    }

    public function getUserTransactions(int $account_id)
    {
        $body = [
            'account_id' => $account_id
        ];

        return $this->makeCallGuzzle('GET', 'user_transactions', $body);
    }


    public function createWithdrawalRequest(int $account_id, $wallet_id,  float $amount, string $address, $create = null)
    {
        $body = [
            'account_id' => $account_id,
            'wallet_id'  => $wallet_id,
            'amount'     => $amount,
            'address'    => $address,
            'create'     => $create,
        ];

        return $this->makeCallGuzzle('POST', 'create_withdrawal', $body);
    }

    public function getWithdrawalRequests(string $status = null, string $search_query = null)
    {
        $body = [
            'status'       => $status,
            'search_query' => $search_query,
        ];

        return $this->makeCallGuzzle('GET', 'withdrawals', $body);
    }

    public function getUserWithdrawalRequests(int $account_id)
    {
        $body = [
            'account_id' => $account_id
        ];

        return $this->makeCallGuzzle('GET', 'user_withdrawals', $body);
    }

    public function approveWithdrawalRequest(int $id, string $phrase = null, string $tx_hash = null, string $tx_hex = null)
    {
        $body = [
            'id'      => $id,
            'phrase'  => $phrase,
            'tx_hash' => $tx_hash,
            'tx_hex'  => $tx_hex,
        ];

        return $this->makeCallGuzzle('PUT', 'approve_withdrawal', $body);
    }

    public function rejectWithdrawalRequest(int $id, string $comment = null)
    {
        $body = [
            'id' => $id,
            'comment' => $comment
        ];

        return $this->makeCallGuzzle('PUT', 'reject_withdrawal', $body);
    }

    public function deleteWithdrawalRequest(int $user_id, int $withdrawal_id)
    {
        $body = [
            'user_id'       => $user_id,
            'withdrawal_id' => $withdrawal_id
        ];

        return $this->makeCallGuzzle('DELETE', 'delete_withdrawal', $body);
    }

    public function getHotColdTransactions(int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'current_page' => $current_page,
            'per_page' => $per_page
        ];

        return $this->makeCallGuzzle('GET', 'hot_cold_transactions', $body);
    }

    public function getHotWalletsData()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'hot_wallets_data', $body);
    }

    public function transferToColdWallet(float $amount, int $asset_id)
    {
        $body = [
            'amount'   => $amount,
            'asset_id' => $asset_id
        ];

        return $this->makeCallGuzzle('POST', 'hot_cold_transfer', $body);
    }

    public function depositHotWallet(
        float $amount,
        int $asset_id,
        string $tx_hash = null,
        string $cold_address = null,
        string $message = null
    )
    {
        $body = [
            'amount'       => $amount,
            'asset_id'     => $asset_id,
            'tx_hash'      => $tx_hash,
            'cold_address' => $cold_address,
            'message'      => $message,
        ];

        return $this->makeCallGuzzle('POST', 'deposit_hot_wallet', $body);
    }

    public function generateHotWalletAddress(string $asset_code)
    {
        $body = [
            'asset_code' => $asset_code
        ];

        return $this->makeCallGuzzle('GET', 'generate_hot_address', $body);
    }
}