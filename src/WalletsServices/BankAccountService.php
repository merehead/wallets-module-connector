<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/10/18
 * Time: 3:35 PM
 */

namespace MereHead\WalletsModuleConnector\WalletsServices;


trait BankAccountService
{
    public function getBankAccounts(int $account_id)
    {
        $body = [
            'account_id' => $account_id,
        ];

        return $this->makeCallGuzzle('GET', 'bank_account', $body);
    }

    public function getBankSettings()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'bank_settings', $body);
    }

    public function bankSettingsUpdate(array $data)
    {
        $body = [
            'data' => $data,
        ];

        return $this->makeCallGuzzle('PUT', 'bank_settings', $body);
    }

    public function bankAccountStore(array $data)
    {
        $body = [
            'data' => $data,
        ];

        return $this->makeCallGuzzle('POST', 'bank_account', $body);
    }

    public function bankAccountUpdate(array $data)
    {
        $body = [
            'data' => $data,
        ];

        return $this->makeCallGuzzle('PUT', 'bank_account', $body);
    }

    public function bankAccountDelete(array $data) {
        $body = [
            'data' => $data,
        ];

        return $this->makeCallGuzzle('DELETE', 'bank_account', $body);
    }

}