<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/8/18
 * Time: 3:13 PM
 */

namespace MereHead\WalletsModuleConnector\WalletsServices;


trait UserService
{
    /**
     * Command for listening : create_account
     * Create user account
     * @param int $user_id
     * @return mixed
     */
    public function createAccount(int $user_id)
    {
        $body = [
            'account_id' => $user_id,
        ];

        return $this->makeCallGuzzle('POST', 'account', $body);
    }

    /**
     *  Command for listening : create_user_fee
     * Create users fee
     * @param $user_id
     * @return array
     */
    public function createUserFee($user_id)
    {
        $body = [
            'account_id' => $user_id,
        ];

        return $this->makeCallGuzzle('POST', 'account_fee', $body);
    }
}