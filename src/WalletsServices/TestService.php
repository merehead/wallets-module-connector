<?php


namespace MereHead\WalletsModuleConnector\WalletsServices;

trait TestService
{
    public function test($testParam)
    {
        $body = [
            'test_param' => $testParam,
        ];

        return $this->makeCallGuzzle('GET', 'test', $body);
    }
}
