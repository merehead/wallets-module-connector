<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/9/18
 * Time: 6:28 PM
 */

namespace MereHead\WalletsModuleConnector\WalletsServices;


trait ProfitService
{
    public function getProfit()
    {
        $body = [];
        return $this->makeCallGuzzle('GET', 'total_profit', $body);
    }

    public function getProfitsList(string $code, $current_page = 0, $per_page = 15)
    {
        $body = [
            'code'         => $code,
            'current_page' => $current_page,
            'per_page'     => $per_page,
        ];

        return $this->makeCallGuzzle('GET', 'total_profits_list', $body);
    }

    public function getProfitTotalByPeriods(string $code)
    {
        $body = [
            'code' => $code
        ];

        return $this->makeCallGuzzle('GET', 'total_profits_asset', $body);
    }
}
