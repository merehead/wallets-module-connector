<?php

namespace  MereHead\WalletsModuleConnector\Modules;


use MereHead\WalletsModuleConnector\WalletsServices\CurrencyService;
use MereHead\WalletsModuleConnector\WalletsServices\TestService;
use MereHead\WalletsModuleConnector\WalletsServices\BankAccountService;
use MereHead\WalletsModuleConnector\WalletsServices\ExchangeService;
use MereHead\WalletsModuleConnector\WalletsServices\UserService;
use MereHead\WalletsModuleConnector\WalletsServices\WalletsService;
use MereHead\WalletsModuleConnector\WalletsServices\CardService;
use MereHead\WalletsModuleConnector\WalletsServices\ProfitService;
use MereHead\WalletsModuleConnector\WalletsServices\InvoiceService;


class WalletsModuleService extends WalletsConnectionModule
{

    use WalletsService, BankAccountService, CardService, UserService, ExchangeService, ProfitService, InvoiceService, TestService, CurrencyService;

    /**
     * Command for listening : ping
     * Pinging trade module server
     * @return mixed
     */
    public function ping() {
        $msg = [
            'command' => 'ping',
        ];
        return $this->makeCall($msg);
    }

}