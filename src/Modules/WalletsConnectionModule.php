<?php

namespace  MereHead\WalletsModuleConnector\Modules;

use GuzzleHttp\Client;

/**
 * Class WalletsConnectionModule
 * @package App\Services\Modules
 */
class WalletsConnectionModule
{
    protected $requester;
    protected $guzzleClient;
    protected $connected = false;
    private $encrypter;

    public function __construct()
    {
        if(config('wallets_module.encryption_key')){
            $this->encrypter = new \Illuminate\Encryption\Encrypter(config('wallets_module.encryption_key'), 'AES-256-CBC');
        }

        $this->guzzleClient = new Client([
            // Base URI is used with relative requests
            'base_uri' => config('wallets_module.address'),
        ]);
    }

    public function makeCallGuzzle(string $method, string $url, array $data)
    {
        $body = [];
        $data = json_encode($data);
        if(config('wallets_module.encryption_key')){
            $data = $this->encrypter->encrypt($data);
        }

        $body['encrypted_data'] = $data;

        try {
            $response = $this->guzzleClient->request(
                $method,
                $url,
                [
                    'headers' => ['content-type' => 'application/json'],
                    'body'    => json_encode($body),
                ]
            );

            return  json_decode($response->getBody(), 1);

        } catch(\Exception $e) {
            $response = $e->getResponse();

            if (null !== $response) {
//                $res  = $response->getBody()->getContents();
//                //Cheat to forward exceptions to main site
//                if (env('APP_DEBUG')) {
//                    dd($res);
//                }
                dd('error in wallet module');
            }
        }
    }
}