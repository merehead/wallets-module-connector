<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 7/26/18
 * Time: 12:46 PM
 */

namespace MereHead\WalletsModuleConnector;


use Illuminate\Support\ServiceProvider;
use MereHead\WalletsModuleConnector\Modules\WalletsModuleService;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $config = __DIR__.'/Config/config.php';

        $this->publishes([
            $config => config_path('wallets_module.php'),
        ], 'config');

        $this->mergeConfigFrom($config, 'wallets_module');
    }


    /**
     * @throws \Exception
     */
    public function boot() {
        $this->app->bind('walletsmodule', WalletsModuleService::class);
    }

}