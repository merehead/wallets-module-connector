#Installation

Add package repository to repositories list in composer.json

        "repositories": [
              .....
              .....

              {
                "type": "vcs",
                "url": "https://git@bitbucket.org/merehead/wallets-module-connector.git"
              },

              .....
              .....
            ]

Require this package
        
        composer require merehead/wallets-module-connector

After installing the package, open your Laravel config file located at config/app.php
and add the following lines in the $providers and $aliases arrays

    'providers' => [
            .....
            .....
            
            MereHead\\WalletsModuleConnector\\ModuleServiceProvider::class,
            
            .....
            .....
      ]
      'aliases' => [
            .....
            .....
            
            'WalletsModule' => MereHead\WalletsModuleConnector\Helpers\Facades\WalletsModule::class,
      
            .....
            .....
      ]
## Publishing config
 
If you want to edit config you need to run

    php artisan vendor:publish --provider="merehead/wallets-module-connector" --tag=config 

    php artisan vendor:publish

So config file will be moved to /config/wallets_module.php and can be edited as you want and changes will not be lost after composer update.